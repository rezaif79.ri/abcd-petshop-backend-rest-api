'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class addresses extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            addresses.belongsTo(models.profiles, { foreignKey: 'user_id', as: 'user' })
            addresses.hasMany(models.orders, { as: 'orders', foreignKey: 'address_id' })
        }
    };
    addresses.init({
        user_id: DataTypes.INTEGER,
        name: DataTypes.STRING,
        phone: DataTypes.STRING,
        province: DataTypes.STRING,
        city: DataTypes.STRING,
        postal_code: DataTypes.STRING,
        address: DataTypes.STRING,
        province_code: DataTypes.STRING,
        city_code: DataTypes.STRING,
    }, {
        sequelize,
        modelName: 'addresses',
    });
    return addresses;
};