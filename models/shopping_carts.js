'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class shopping_carts extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            shopping_carts.belongsTo(models.profiles, { foreignKey: 'user_id', as: 'user' })
            shopping_carts.belongsTo(models.products, { foreignKey: 'product_id', as: 'product' });
        }
    };
    shopping_carts.init({
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        user_id: DataTypes.INTEGER,
        product_id: DataTypes.INTEGER,
        quantity: DataTypes.INTEGER
    }, {
        sequelize,
        modelName: 'shopping_carts',
    });
    return shopping_carts;
};