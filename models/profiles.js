'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class profiles extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            profiles.belongsTo(models.roles, { foreignKey: 'role_id', as: 'role' })
            profiles.hasMany(models.addresses, { foreignKey: 'user_id', as: 'addresses' })
            profiles.hasMany(models.orders, { foreignKey: 'user_id', as: 'orders' })
            profiles.hasMany(models.user_chats, { foreignKey: 'user_id', as: 'sent_chats' })
            profiles.hasMany(models.user_chats, { foreignKey: 'user_id', as: 'received_chats' })
            profiles.belongsToMany(models.products, { through: 'shopping_carts', foreignKey: 'user_id', as: 'products' })
        }
    };
    profiles.init({
        email: DataTypes.STRING,
        name: DataTypes.STRING,
        user_id: DataTypes.STRING,
        // password: DataTypes.STRING,
        phone: DataTypes.STRING,
        // access_token: DataTypes.STRING,
        // refresh_token: DataTypes.STRING,
        role_id: DataTypes.INTEGER
    }, {
        sequelize,
        modelName: 'profiles',
    });
    return profiles;
};