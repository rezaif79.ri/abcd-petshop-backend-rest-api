'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class product_order_histories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      product_order_histories.belongsTo(models.order_histories, {foreignKey: 'order_history_id'})
    }
  };
  product_order_histories.init({
    order_history_id: DataTypes.INTEGER,
    product_img: DataTypes.STRING,
    product_name: DataTypes.STRING,
    product_price: DataTypes.FLOAT,
    product_weight: DataTypes.FLOAT,
    qty: DataTypes.INTEGER,
    sub_weight: DataTypes.FLOAT,
    sub_total: DataTypes.FLOAT
  }, {
    sequelize,
    modelName: 'product_order_histories',
  });
  return product_order_histories;
};