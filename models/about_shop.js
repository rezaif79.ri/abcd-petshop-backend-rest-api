'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class about_shop extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  about_shop.init({
    name: DataTypes.STRING,
    img: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    description: DataTypes.TEXT,
    maps: DataTypes.TEXT,
    address: DataTypes.TEXT,
    province: DataTypes.STRING,
    city: DataTypes.STRING,
    postal_code: DataTypes.STRING,
    province_code: DataTypes.STRING,
    city_code: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'about_shop',
  });
  return about_shop;
};