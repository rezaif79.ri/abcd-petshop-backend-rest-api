'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class order_histories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      order_histories.hasMany(models.product_order_histories, {foreignKey: 'order_history_id', as: 'products'})
    }
  };
  order_histories.init({
    user_name: DataTypes.STRING,
    user_phone: DataTypes.STRING,
    user_province: DataTypes.STRING,
    user_city: DataTypes.STRING,
    user_postal_code: DataTypes.STRING,
    user_address: DataTypes.STRING,
    grand_weight: DataTypes.FLOAT,
    grand_total: DataTypes.FLOAT
  }, {
    sequelize,
    modelName: 'order_histories',
  });
  return order_histories;
};