'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class products extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            products.hasMany(models.supplies, { foreignKey: 'product_id', as: 'supplies' })
            products.belongsToMany(models.tags, { through: 'product_tags', foreignKey: 'product_id', as: 'tags' })
            products.belongsToMany(models.profiles, { through: 'shopping_carts', foreignKey: 'product_id', as: 'users' })
            products.belongsToMany(models.orders, { through: 'order_products', foreignKey: 'product_id', as: 'orders' })
            // products.belongsTo(models.order_products, {foreignKey: 'product_id', as: 'order_products'})
        }
    };
    products.init({
        name: DataTypes.STRING,
        description: DataTypes.TEXT,
        qty: DataTypes.INTEGER,
        price: DataTypes.FLOAT,
        img: DataTypes.STRING,
        weight: DataTypes.INTEGER,
        sold: DataTypes.INTEGER
    }, {
        sequelize,
        modelName: 'products',
    });
    return products;
};