'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class orders extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            orders.belongsTo(models.profiles, { foreignKey: 'user_id', as: 'user' })
            orders.belongsTo(models.addresses, { foreignKey: 'address_id', as: 'address' })
            orders.belongsToMany(models.products, { through: 'order_products', foreignKey: 'order_id', as: 'products' })
            orders.hasMany(models.order_products, {foreignKey: 'order_id', as:'order_product'})
        }
    };
    orders.init({
        user_id: DataTypes.INTEGER,
        address_id: DataTypes.INTEGER,
        shipping_costs: DataTypes.FLOAT,
        grand_total: DataTypes.FLOAT,
        grand_weight: DataTypes.INTEGER,
        resi: DataTypes.STRING,
        status: DataTypes.STRING,
        midtrans_order_id: DataTypes.STRING,
        midtrans_snap_token: DataTypes.STRING
    }, {
        sequelize,
        modelName: 'orders',
    });
    return orders;
};