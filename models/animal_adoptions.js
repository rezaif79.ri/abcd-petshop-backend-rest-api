'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class animal_adoptions extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      animal_adoptions.belongsTo(models.categories, {foreignKey: 'categoryId', as: 'category'})
    }
  };
  animal_adoptions.init({
    name: DataTypes.STRING,
    img: DataTypes.STRING,
    age: DataTypes.INTEGER,
    animal_race: DataTypes.STRING,
    categoryId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'animal_adoptions',
  });
  return animal_adoptions;
};