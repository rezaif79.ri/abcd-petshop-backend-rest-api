'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class user_chats extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            user_chats.belongsTo(models.profiles, { targetKey: 'user_id', foreignKey: 'sender_id', as: 'sender' })
            user_chats.belongsTo(models.profiles, { targetKey: 'user_id', foreignKey: 'recipient_id', as: 'recipient' })
        }
    };
    user_chats.init({
        sender_id: DataTypes.INTEGER,
        recipient_id: DataTypes.INTEGER,
        message: DataTypes.TEXT
    }, {
        sequelize,
        modelName: 'user_chats',
    });
    return user_chats;
};