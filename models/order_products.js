'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class order_products extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            order_products.belongsTo(models.products, { foreignKey: 'product_id', as: 'product' })
            order_products.belongsTo(models.orders, { foreignKey: 'order_id', as: 'order' })
        }
    };
    order_products.init({
        product_id: DataTypes.INTEGER,
        order_id: DataTypes.INTEGER,
        qty: DataTypes.INTEGER,
        sub_total: DataTypes.FLOAT,
        sub_weight: DataTypes.INTEGER
    }, {
        sequelize,
        modelName: 'order_products',
    });
    return order_products;
};