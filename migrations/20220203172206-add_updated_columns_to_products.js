'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addColumn('products', 'weight', { type: Sequelize.INTEGER });
        await queryInterface.addColumn('products', 'sold', { type: Sequelize.INTEGER });
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
    }
};