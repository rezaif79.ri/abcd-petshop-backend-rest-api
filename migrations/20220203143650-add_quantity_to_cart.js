'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        queryInterface.addColumn('shopping_carts', 'quantity', { type: Sequelize.INTEGER });
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
    }
};