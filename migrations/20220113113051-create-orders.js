'use strict';
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable('orders', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            user_id: {
                type: Sequelize.STRING,
            },
            address_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: "addresses",
                    key: "id",
                },
            },
            shipping_costs: {
                type: Sequelize.FLOAT.UNSIGNED,
                defaultValue: 0
            },
            grand_total: {
                type: Sequelize.FLOAT.UNSIGNED,
                defaultValue: 0
            },
            midtrans_order_id: {
                type: Sequelize.STRING,
                defaultValue: ""
            },
            midtrans_snap_token: {
                type: Sequelize.STRING,
                defaultValue: ""
            },
            resi: {
                type: Sequelize.STRING,
                defaultValue: ""
            },
            status: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable('orders');
    }
};