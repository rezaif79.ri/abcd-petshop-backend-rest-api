'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('order_histories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      user_phone: {
        type: Sequelize.STRING
      },
      user_province: {
        type: Sequelize.STRING
      },
      user_city: {
        type: Sequelize.STRING
      },
      user_postal_code: {
        type: Sequelize.STRING
      },
      user_address: {
        type: Sequelize.STRING
      },
      grand_weight: {
        type: Sequelize.FLOAT,
        defaultValue: 0
      },
      grand_total: {
        type: Sequelize.FLOAT,
        defaultValue: 0
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('order_histories');
  }
};