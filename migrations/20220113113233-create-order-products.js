'use strict';
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable('order_products', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            product_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: "products",
                    key: "id",
                },
            },
            order_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: "orders",
                    key: "id",
                },
                onDelete: "CASCADE"
            },
            qty: {
                type: Sequelize.INTEGER.UNSIGNED,
                defaultValue: 0
            },
            sub_total: {
                type: Sequelize.FLOAT.UNSIGNED,
                defaultValue: 0
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable('order_products');
    }
};