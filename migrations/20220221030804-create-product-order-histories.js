'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('product_order_histories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      order_history_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "order_histories",
          key: "id",
        },
        onDelete: "CASCADE"
      },
      product_img: {
        type: Sequelize.STRING
      },
      product_name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      product_price: {
        type: Sequelize.FLOAT
      },
      product_weight: {
        type: Sequelize.FLOAT
      },
      qty: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      sub_weight: {
        type: Sequelize.FLOAT,
        defaultValue: 0
      },
      sub_total: {
        type: Sequelize.FLOAT,
        defaultValue: 0
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('product_order_histories');
  }
};