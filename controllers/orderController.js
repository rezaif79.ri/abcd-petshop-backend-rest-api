const { orders, addresses, products, order_products } = require('../models')
const { receiveMidtransNotification } = require("../services/midtransService");
const { initialOrderService, cancelOrderService, createOrderService, getUserOrdersService, getOrderDetailsService } = require("../services/orderService");
const { validateSignature } = require("../utils/midtrans");
const { response_internal_server_error, response_success, response_created } = require("../utils/response")
const { Op } = require("sequelize");

const responseOrders = (o) => {
    return {
        id: o.id,
        user_id: o.user_id,
        address: {
            id: o.address.id,
            name: o.address.name,
            phone: o.address.phone,
            province: o.address.province,
            city: o.address.city,
            postal_code: o.address.postal_code,
            address: o.address.address,
            province_code: o.address.province_code,
            city_code: o.address.city_code
        },
        shipping_costs: o.shipping_costs,
        grand_total: o.grand_total,
        grand_weight: o.grand_weight,
        resi: o.resi,
        status: o.status,
        midtrans_order_id: o.midtrans_order_id,
        midtrans_snap_token: o.midtrans_snap_token
    }
}

const initOrderController = async(req, res) => {
    try {
        const { userId } = res.locals;
        const { status, error, data } = await initialOrderService(userId);
        if (!status) {
            throw new Error(error.message);
        }
        return response_success(res, data);
    } catch (err) {
        return response_internal_server_error(res, err.message);
    }
}

const cancelOrderController = async(req, res) => {
    try {
        const { userId } = res.locals;
        const { orderId } = req.params;
        const { status, error, data } = await cancelOrderService(userId, orderId);
        if (!status) {
            throw new Error(error.message);
        }
        return response_success(res, data);
    } catch (err) {
        return response_internal_server_error(res, err.message);
    }
}

const createOrderController = async(req, res) => {
    try {
        const { userId, email, name } = res.locals;
        const { address_id, shipping_costs } = req.body;

        const init = await initialOrderService(userId);
        if (!init.status) {
            throw new Error(error.message);
        }

        const orderData = {
            user_id: userId,
            address_id,
            shipping_costs,
            email,
            name
        };

        const { status, error, data } = await createOrderService(orderData);
        if (!status) {
            throw new Error(error.message);
        }
        return response_success(res, data);
    } catch (err) {
        return response_internal_server_error(res, err.message);
    }
}

const midtransNotificationController = async(req, res) => {
    try {

        const { order_id } = req.body;
        const { status, data, error } = await receiveMidtransNotification(order_id);
        if (!status) {
            throw new Error(error.message);
        }
        return response_success(res, data);
    } catch (err) {
        return response_internal_server_error(res, err.message);
    }
}

const getUserOrdersController = async(req, res) => {
    try {
        const { userId } = res.locals;
        const { data, status, error } = await getUserOrdersService(userId);
        if (!status) {
            throw new Error(error.message);
        }
        return response_success(res, data);
    } catch (err) {
        return response_internal_server_error(res, err.message);
    }
}

const getOrderDetailsController = async(req, res) => {
    try {
        const { id } = req.params;
        const { userId } = res.locals;
        const { data, status, error } = await getOrderDetailsService(id, userId);
        if (!status) {
            throw new Error(error.message);
        }
        return response_success(res, data);
    } catch (err) {
        return response_internal_server_error(res, err.message);
    }
}

const statusOrder = async(req, res) => {
    try {
        const { id } = req.params;
        const { user_id } = res.locals;
        const order = await orders.findOne({ where: { id, user_id, status: 'DELIVERED' } })

        if (order == null) return response_success(res, 'Waiting for status order DELIVERED')

        await orders.update({
            status: 'FINISH'
        }, {
            where: { id, user_id, status: 'DELIVERED' }
        })

        response_created(res, 'Order accepted!')
    } catch (err) {
        return response_internal_server_error(
            res,
            "Internal Server Error",
            err.message
        );
    }
}

const getOrders = async(req, res) => {
    try {
        const page = req.query.page || 1;
        const offset = (page - 1) * 10;
        const status = req.query.status || '';
        const name = req.query.name || '';

        const filterName = {
            model: addresses,
            as: 'address',
            // through: { where: {name: {[Op.iLike]: `%${name}%`}}}
            where: {
                name: {
                    [Op.iLike]: `%${name}%`
                }
            }
        }

        const order = await orders.findAll({
            where: {
                status: {
                    [Op.iLike]: `%${status}%`
                }
            },
            order: [
                ['id', 'DESC'],
            ],
            include: filterName,
            offset,
            limit: 10
        })

        const count = await orders.findAndCountAll({
            where: {
                status: {
                    [Op.iLike]: `%${status}%`
                }
            },
            include: filterName,
        })

        // console.log(order)        
        // const data = order.map(e => {
        //     return responseOrders(e);
        // })

        response_success(res, { count: count.count, orders: order })
    } catch (err) {
        return response_internal_server_error(
            res,
            "Internal Server Error",
            err.message
        );
    }
}

const getOrderById = async(req, res) => {
    try {
        const { orderId } = req.params;

        const order = await orders.findOne({
            where: { id: orderId },
            include: [{
                model: addresses,
                as: "address"
            }, {
                model: order_products,
                include: ['product'],
                as: 'order_product'
            }],
        })

        response_success(res, order)

    } catch (err) {
        return response_internal_server_error(
            res,
            "Internal Server Error",
            err.message
        );
    }
}

const updateResi = async(req, res) => {
    try {
        const { orderId } = req.params
        const { resi, status } = req.body
        const order = await orders.findOne({ where: { id: orderId } })

        if (order == null) return response_success(res, "id orders not found!")
            // if(resi == "") return response_success(res, "please input resi!")

        await orders.update({
            resi,
            status
        }, {
            where: { id: orderId }
        })

        response_created(res, "receipt has been added")
    } catch (err) {
        return response_internal_server_error(
            res,
            "Internal Server Error",
            err.message
        );
    }
}

const deleteOrder = async(req, res) => {
    try {
        const { orderId } = req.params

        const order = await orders.findOne({ where: { id: orderId } })

        if (order == null) return response_success(res, "id orders not found!")

        await orders.destroy({
            where: { id: orderId }
        })

        response_success(res, "deleted!")
    } catch (err) {
        return response_internal_server_error(
            res,
            "Internal Server Error",
            err.message
        );
    }
}

module.exports = {
    initOrderController,
    createOrderController,
    cancelOrderController,
    createOrderController,
    midtransNotificationController,
    getOrderDetailsController,
    getUserOrdersController,
    statusOrder,
    getOrders,
    getOrderById,
    updateResi,
    deleteOrder
}