const { loginService } = require("../services/authService");
const { response_internal_server_error, response_created, response_success, response_unauthorized } = require("../utils/response")

const registerController = async(req, res) => {
    try {
        const { userId, email, name } = res.locals;

        const { status, data, err } = await loginService({ userId, email, name });
        if (!status) {
            throw new Error(err.message);
        }

        return response_created(res, { user: data.user });
    } catch (err) {
        return response_internal_server_error(res, err.message);
    }

}

const checkRoleController = async(req, res) => {
    return response_success(res, "User is authorized to access this endpoint/page!");
}

module.exports = { registerController, checkRoleController };