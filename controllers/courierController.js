const { Op } = require("sequelize");
const {couriers} = require("../models")
const {response_success, response_internal_server_error, response_created} = require('../utils/response')
// return all data
exports.index = async (req, res) => {
    const title = !!req.query.title ? req.query.title : "";

    const page = req.query.page || 1;

    const offset = (page-1) * 20;

    const count = await couriers.findAndCountAll({
        where: {
            name: {
                [Op.iLike]: `%${title}%`
            }
        }
    })

    await couriers.findAll({
        order: [
            ['id', 'DESC'],
        ],
        offset: offset,
        limit : 20,
        where: {
            name: {
                [Op.iLike]: `%${title}%`
            }
        }
    })
    .then(result => {
        response_success(res, {count:count.count, couriers: result})
    })
    .catch(err => {
        // console.log(err)
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

//get specific data
exports.show =async (req, res) => {
    const id = req.params.id
    await couriers.findByPk(id)
    .then(result => {
        response_success(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// post data
exports.store = async (req, res) => {
    const {name, courier_code} = req.body
    await couriers.create({
        name:name,
        courier_code:courier_code
    })
    .then(result => {
        response_created(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// edit specific data
exports.update = async (req, res) => {
    const id = req.params.id
    const {name, courier_code} = req.body

    await couriers.update({
        name:name,
        courier_code:courier_code
    },
    {
        where: {
            id: id
        },
        returning: true
    })
    .then(result => {
        response_success(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// delete certain data
exports.destroy = async (req, res) => {
    const id = req.params.id

    await couriers.destroy({
        where: {
            id:id
        }
    })
    .then(result => {
        response_success(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}