const { addToCartService, getUserShoppingCartService, deleteShoppingCartService, updateCartQuantityService } = require("../services/cartService");
const { response_success, response_internal_server_error, response_unauthorized } = require("../utils/response");

const addToShoppingCartController = async(req, res) => {
    try {
        const { userId } = res.locals;
        const { product_id, quantity } = req.body;
        const cartData = {
            userId,
            product_id,
            quantity
        }
        const { status, error, data } = await addToCartService(cartData);
        if (status == false) {
            throw new Error(error.message);
        }
        return response_success(res, data);
    } catch (err) {
        return response_internal_server_error(res, err.message);
    }
}

const getUserShoppingCartController = async(_, res) => {
    try {
        const { userId } = res.locals;

        const { status, error, data } = await getUserShoppingCartService(userId);
        if (status == false) {
            throw new Error(error.message);
        }
        return response_success(res, data);
    } catch (err) {
        return response_internal_server_error(res, err.message);
    }
}

const updateCartQuantityController = async(req, res) => {
    try {
        const { userId } = res.locals;
        const { cart_id, method } = req.body;
        const { status, error, data } = await updateCartQuantityService(cart_id, userId, method);
        if (status == false) {
            throw new Error(error.message);
        }
        return response_success(res, data);
    } catch (err) {
        return response_internal_server_error(res, err.message);
    }
}

const deleteShoppingCartController = async(req, res) => {
    try {
        const { cart_id } = req.params;
        const { userId } = res.locals;
        const { status, error, data } = await deleteShoppingCartService(cart_id, userId);
        if (status == false) {
            throw new Error(error.message);
        }
        return response_success(res, data, "Successfuly removed an item from shopping cart!");
    } catch (err) {
        if (err.message == "Unauthorized") {
            return response_unauthorized(res, "This cart item doesn't belong to this user!");
        }
        return response_internal_server_error(res, err.message);
    }
}

module.exports = {
    addToShoppingCartController,
    getUserShoppingCartController,
    deleteShoppingCartController,
    updateCartQuantityController
}