const { Op } = require("sequelize");
const {supplies, products} = require("../models")
// const products = require("../models/products")
const {response_success, response_internal_server_error, response_created} = require('../utils/response')

// return all data
exports.index = async (req, res) => {
    const page = req.query.page || 1;
    // const title = !!req.query.title ? req.query.title : "";
    const checkStartDate = req.query.startDate || false
    const checkEndDate = req.query.endDate || false;
    const startDate = req.query.startDate || new Date().toISOString()
    const endDate = req.query.endDate || new Date().toISOString();
    let query = {}

    if(!!checkStartDate || !!checkEndDate){
        query = {
            createdAt : {
                [Op.gte]: startDate,
                [Op.lte]: endDate 
            }
        }
    }
    const count = await supplies.findAndCountAll({
        where: query
    })

    const offset = (page-1) * 20;

    await supplies.findAll({
        include: ['product'],
        order: [
            ['id', 'DESC'],
        ],
        offset: offset,
        limit : 20,
        where: query
    })
    .then(result => {
        response_success(res, {count:count.count, supplies: result})
    })
    .catch(err => {
        console.log(err)
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// post data
exports.store = async (req, res) => {
    try{

        const {product_id, qty} = req.body
        const product = await products.findByPk(product_id)
        await products.update({
            qty: parseInt(product.qty) + parseInt(qty)
        }, {
            where: {
                id:product_id
            }
        })

        await supplies.create({
            product_id: product_id,
            qty: qty
        },
        {
            include: ['product'],
        }).then(result => {
            response_created(res, result)
        })
    

    } catch(err) {
        console.log(err)
        response_internal_server_error(res, 'Internal Server Error', err)
    }
}

// return specific data
exports.show = async (req, res) => {
    const id = req.params.id

    await supplies.findOne({
        include: ['product'],
        where: {
            id: id
        }
    })
    .then(result => {
        response_success(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// edit specific data
exports.update = async (req, res) => {
    const id = req.params.id
    const {product_id, qty} = req.body

    await supplies.update({
        // product_id: product_id,
        qty: qty
    },
    {
        include: ['product'],
        where: {
            id: id
        },
        returning: true
    })
    .then(result => {
        response_success(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// delete certain data
exports.destroy = async (req, res) =>{
    const id = req.params.id

    await supplies.destroy({
        where: {
            id: id
        }
    })
    .then(result => {
        response_success(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}