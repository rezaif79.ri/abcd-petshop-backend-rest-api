const { Op, QueryTypes } = require("sequelize");
const {response_success, response_internal_server_error, response_created} = require('../utils/response')
const {animal_adoptions} = require('../models');

// return all data
exports.index = async (req, res) => {
    const title = req.query.title ? req.query.title : ''
    const category = req.query.category 
    const page = req.query.page || 1;
    const limit = req.query.limit || 10;

    const offset = (page-1) * limit;

    let condition = {
        name:{
        [Op.iLike]: `%${title}%`
    }}

    if(category){
        condition = {
            name:{
                [Op.iLike]: `%${title}%`
            },
            categoryId: category
        }
    } 

    const params = {
        include: ["category"],
        where: condition,
        order: [
            ['id'],
        ],
        offset: offset,
        limit : limit
    }

    try {      
        const all_adopt = await animal_adoptions.findAll(params);
        params.offset = (page) * limit;
        const adoptIndex = await animal_adoptions.findAndCountAll(params)
        
        return res.status(200).json({
            message: "OK",
            count: all_adopt.length,
            page: page,
            limit: limit,
            endIndex: adoptIndex.rows.length,
            data: {
                all_adopt
            }
        });

    } catch (err) {
        response_internal_server_error(res, 'Internal Server Error', err)
    }
}

// post data
exports.store = async (req, res) => {
    const {name, img = null, age, animal_race, categoryId} = req.body
    await animal_adoptions.create({
        name: name,
        img: img,
        age: age,
        animal_race: animal_race,
        categoryId: categoryId
    }, {
        returning: true
    }).then((result) => {
        response_created(res, result)
    }).catch((err)=> {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// return specific data
exports.show = async (req, res) => {
    const id = req.params.id
    
    try {      
        let all_adopt = await animal_adoptions.findOne({
            include: ["category"],
            where: {
                id: id
            }
        })

        response_success(res, all_adopt)
    } catch (err) {
        response_internal_server_error(res, 'Internal Server Error', err)
    }
}

// edit specific data
exports.update = async (req, res) => {
    const id = req.params.id
    const {name, img, age, animal_race, categoryId} = req.body
    await animal_adoptions.update({
        name: name,
        img: img,
        age: age,
        animal_race: animal_race,
        categoryId: categoryId
    }, {
        where: {
            id: id
        },
        returning: true
    }).then((result) => {
        response_success(res, result)
    }).catch((err)=> {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// delete certain data
exports.destroy = async (req, res) => {
    const id = req.params.id

    await animal_adoptions.destroy({
        where: {
            id: id
        }
    }).then( (result) => {
        response_success(res, result)
    }).catch( err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}