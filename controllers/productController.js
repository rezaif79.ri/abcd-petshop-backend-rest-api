const { products, product_tags, tags, shopping_carts, supplies, order_products } = require('../models');
const { Op } = require("sequelize");

const {response_success, response_internal_server_error, response_created, response_not_found, response_conflict} = require('../utils/response');

const filterProduct = (product) => {
    return {
        id: product.id,
        product_id: {
            id: product.id,
            name: product.name,
            description: product.description,
            qty: product.qty,
            price: product.price,
            img: product.img,
            weight: product.weight,
            sold: product.sold
        },
        tags_id: product.tags?.map((tags) => {
            const category = {
                id: tags.id,
                name: tags.name
            }
            return category;
        })
    }
}

const productControllers = {
    async getAllProduct(req, res){
        try {
            const min_price = req.query.min_price || 0;
            const max_price = req.query.max_price || 9999999999;
            const title = req.query.title || "";
            const tags_query = req.query.tags || "";
            const redirect = req.query.redirect || 1;
            const limit = parseInt(req.query.limit || 10);
            const page = parseInt(req.query.page || 1);
            const best_seller = req.query.best || 0;

            if(redirect === 1){
                return res.redirect(`/products?title=${title}&min_price=${min_price}&max_price=${max_price}&tags=${tags_query}&limit=${limit}&page=${page}&best=${best_seller}&redirect=0`)
            }

            const offset = 0 + (page - 1) * limit;
            const tagsInclude = tags_query.length <= 0 ? {
                model: tags, 
                as: "tags",
            }:{
                model: tags, 
                as: "tags", 
                where:{
                    name: {[Op.iLike]: `%${tags_query}%`}
                }
            }
            const orderBy = (best_seller == 1) ? [['sold', 'DESC']] : [['updatedAt', 'DESC']]
            const params = {
                where:{
                    name: {[Op.iLike]: `%${title}%`},
                    [Op.and]: [
                        { price: { [Op.gt]: min_price } },
                        { price: { [Op.lt]: max_price } }
                    ]
                },
                include: tagsInclude,
                order: orderBy,
                limit,
                offset
            }
            const allProduct = await products.findAll(params);

            params.offset = 0 + (page) * limit;
            tags_query.length <= 0 ? params.include = null : true;
            
            const endProducts = await products.findAndCountAll(params);

            const productList = allProduct.map((row) => {
                const data = filterProduct(row);
                return data;
            });

            return res.status(200).json({
                message: "OK",
                count: productList.length, 
                page: page, 
                limit: limit,
                endIndex: endProducts.rows.length, 
                data:{
                    products: productList
                }
            });
        } catch (e) {
            return response_internal_server_error(res, 'Internal Server Error', e.message);
        }
    },
    
    async getProductById(req, res){
        try {
            const { productId } = req.params;

            const getProduct = await products.findByPk(productId, {
                include: { model: tags, as: "tags" }
            });

            const product = filterProduct(getProduct);
            
            return response_success(res, product);
        } catch (e) {
            return response_internal_server_error(res, 'Internal Server Error', e.message);
        }
    },

    async postNewProduct(req, res){
        try {
            const { name, description, qty, price, img, weight, sold, tags_id} = req.body;
            const newProduct = await products.create({
                name, description, qty, price, img, weight, sold
            });
            
            if(!!tags_id){
                const promiseTags = tags_id.map(async(tag) => {
                    return await product_tags.create({
                        product_id: newProduct.id,
                        tag_id: tag
                    });
                });
    
                Promise.all(promiseTags).then(async()=>{
                    const getProduct = await products.findByPk(newProduct.id, {
                        include: { model: tags, as: "tags" }
                    });
        
                    const product = filterProduct(getProduct);
                    return response_created(res, product);
                    }).catch((err) =>{
                        return response_internal_server_error(res, 'Internal Server Error', err.message);
                    });
            }
            
            return response_created(res, filterProduct(newProduct));
        } catch (e) {
            console.log(e);
            return response_internal_server_error(res, 'Internal Server Error', e.message);            
        }
    },
    async updateProductById(req, res){
        try {
            const { productId } = req.params;
            const { name, description, qty, price, img, weight, sold, tags_id} = req.body;

            const getProduct = await products.findByPk(productId, {
                include: { model: tags, as: "tags" }
            });

            const updateProduct = await getProduct.update({
                name, description, qty, price, img, weight, sold
            });

            if(!!tags_id){
                const promiseTags = tags_id.map(async(tag) => {
                    return await product_tags.create({
                        product_id: updateProduct.id,
                        tag_id: tag
                    });
                });

                await product_tags.destroy({
                    where:{
                        product_id: productId
                    }, cascade: true
                }).then(() => {
                     Promise.all(promiseTags).then(async()=>{
                        const getProduct = await products.findByPk(updateProduct.id, {
                            include: { model: tags, as: "tags" }
                        });
            
                        const product = filterProduct(getProduct);
                        return response_created(res, product);
                    }).catch((err) =>{
                        return response_internal_server_error(res, 'Internal Server Error', err.message);
                    });
                });
            }else{
                const product = filterProduct(updateProduct);
                return response_success(res, product);
            }
            
            
        } catch (err) {
            response_internal_server_error(res, 'Internal Server Error', e.message);
        }
    },
    async deleteProductById(req, res){
        try {
            const { productId } = req.params;

            const getProduct = await products.findByPk(productId, {
                include: { model: tags, as: "tags" }
            });

            if(!getProduct){
                return response_not_found(res, "Product not found");
            }else{
                const getActiveOrder = await order_products.findOne({where: {product_id: productId}})
                if(!!getActiveOrder){
                    return response_conflict(res, "The product cannot be deleted because it is still in the process of ordering");
                }

                await product_tags.destroy({
                    where:{
                        product_id: productId
                    }, cascade: true
                })
                .then(async() => {
                    await shopping_carts.destroy({
                        where:{
                            product_id: productId
                        },
                        cascade: true
                    });
                })
                .then(async() => {
                    await supplies.destroy({
                        where:{
                            product_id: productId
                        },
                        cascade: true
                    });
                })
                .then(async() => {
                    await order_products.destroy({
                        where:{
                            product_id: productId
                        },
                        cascade: true
                    });
                })
                .then(async() => {
                    await getProduct.destroy({cascade: true}).then(()=>{
                        return response_success(res, {message:"SUCCESS_DELETED!"});
                    });
                });
            }

        } catch (e) {
            console.log(e);
            return response_internal_server_error(res, 'Internal Server Error', e.message);
        }
    }
}
    


module.exports = productControllers;