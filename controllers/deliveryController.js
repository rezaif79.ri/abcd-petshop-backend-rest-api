const request = require("request");
const { response_internal_server_error, response_success } = require("../utils/response");
const {addresses} = require("../models")

require("dotenv").config();

const { DELIVERY_KEY } = process.env;

exports.province = async (req, res) => {
  const options = {
    method: "GET",
    url: "https://api.rajaongkir.com/starter/province",
    headers: { key: DELIVERY_KEY },
  };

  request(options, function (error, response, body) {
    // if (error) throw new Error(error);
    if (error)
      response_internal_server_error(res, "Internal Server Error", error);
    return res.status(200).json(JSON.parse(body));
  });
};

exports.city = async (req, res) => {
  const id = req.query.id ? req.query.id : "";
  const province = req.query.province ? req.query.province : "";
  // console.log(province)

  const options = {
    method: "GET",
    url: "https://api.rajaongkir.com/starter/city",
    qs: { id: id, province: province },
    headers: { key: DELIVERY_KEY },
  };

  request(options, function (error, response, body) {
    // if (error) throw new Error(error);
    if (error)
      response_internal_server_error(res, "Internal Server Error", error);
    return res.status(200).json(JSON.parse(body));
  });
};

exports.cost = async (req, res) => {
  // const origin = req.headers.origin ? req.headers.origin : "112";
  // const destination = req.headers.destination ? req.headers.destination : "";
  // const weight = req.headers.weight ? Number(req.headers.weight) : 0;
  // const courier = req.headers.courier ? req.headers.courier : "";
  const origin = "112";
  const user_address = req.body.address_id ? req.body.address_id : null ;
  const weight = req.body.weight ? Number(req.body.weight) : 0;
  const courier = req.body.courier ? req.body.courier : "jne";

  if(!(!!user_address)) {
    response_success(res, {})
  } else {
    try{
      const destination = await addresses.findByPk(user_address)
  
      const options = {
        method: "POST",
        url: "https://api.rajaongkir.com/starter/cost",
        headers: {
          key: DELIVERY_KEY,
          "content-type": "application/x-www-form-urlencoded",
        },
        form: {
          origin: origin,
          destination: destination.city_code,
          weight: weight,
          courier: courier,
        },
      };
    
      request(options, function (error, response, body) {
        // if (error) throw new Error(error);
        if (error) response_internal_server_error(res, "Internal Server Error", error);
        return res.status(200).json(JSON.parse(body));
      });
  
    } catch(err) {
      response_internal_server_error(res, "Internal Server Error", err);
    }
  }
  

};
