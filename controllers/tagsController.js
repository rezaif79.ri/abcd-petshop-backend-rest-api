const { tags } = require('../models');
const { Op } = require("sequelize");
const {response_success, response_internal_server_error, response_conflict, response_created} = require('../utils/response')

const tagsController = {
    async getAllTags(req, res){
        try {
            const title = !!req.query.title ? req.query.title : "";
            
            const count = await tags.findAndCountAll({
                where: {
                    name: {
                        [Op.iLike]: `%${title}%`
                    }
                }
            })

            const allTags = await tags.findAll({
                where: {
                    name: {
                        [Op.iLike]: `%${title}%`
                    }
                }
            });

            return response_success(res, {count: count.count, tags: allTags});
        } catch (e) {
            return response_internal_server_error(res, 'Internal Server Error', e.message);
        }
    },
    async showTagsById(req,res){
        try {
            const id = req.params.tagsId;
            const Tag = await tags.findByPk(id);

            return response_success(res, Tag);

        } catch (e) {
            return response_internal_server_error(res, 'Internal Server Error', e.message);
        }
    },
    async postNewTags(req, res){
        try {
            const { name } = req.body;
            const oldTags = await tags.findOne({where: {name: name}});
            if(!!oldTags){
                return response_conflict(res, {message: "Tags already exist"});
            }

            const newTags = await tags.create({name});

            return response_created(res, {id: newTags.id, name: newTags.name});

        } catch (e) {
            return response_internal_server_error(res, 'Internal Server Error', e.message);
        }
    },
    async updateTagsById(req, res){
        try {
            const id = req.params.tagsId;
            const { name } = req.body;
            const oldTags = await tags.findOne({where: {id: id}});
            if(!oldTags){
                returnresponse_conflict(res, {message: "Tags not found"});
            }

            const updateTags = await oldTags.update({name})

            return response_success(res, {id: updateTags.id, name: updateTags.name});

        } catch (e) {
            return response_internal_server_error(res, 'Internal Server Error', e.message);
        }
    },
    async deleteTagsById(req, res){
        try {
            const id = req.params.tagsId;
            const oldTags = await tags.findOne({where: {id: id}});
            if(!oldTags){
                return response_conflict(res, {message: "Tags not found"});
            }

            await oldTags.destroy();
            return response_success(res, {message:"SUCCESS_DELETED!"});

        } catch (e) {
            return response_internal_server_error(res, 'Internal Server Error', e.message);
        }
    }
    

}

module.exports = tagsController;