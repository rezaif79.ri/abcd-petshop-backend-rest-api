const { Op } = require("sequelize");
const {categories} = require('../models')
const {response_success, response_created, response_internal_server_error} = require('../utils/response')

// return all data
exports.index = async (req, res) => {
    const title = !!req.query.title ? req.query.title : "";

    const count = await categories.findAndCountAll({
        where: {
            name: {
                [Op.iLike]: `%${title}%`
            }
        }
    })

    try{
        const all_category = await categories.findAll({
            where: {
                name:{
                    [Op.iLike]: `%${title}%`
                }
            }
        })
    
        response_success(res, {count:count.count, categories: all_category})
    } catch (err) {
        response_internal_server_error(res, 'Internal Server Error', err)
    }
}

//get specific data
exports.show = async (req, res) => {
    const id = req.params.id

    await categories.findByPk(id)
    .then(result => {
        response_success(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// post data
exports.store = async (req, res) => {
    const {name} = req.body

    await categories.create({
        name
    })
    .then(result => {
        response_created(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// edit specific data
exports.update = async (req, res) => {
    const id = req.params.id
    const {name} = req.body

    await categories.update({
        name
    }, {
        where: {
            id: id
        },
        returning: true
    })
    .then(result => {
        response_success(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// delete certain data
exports.destroy = async (req, res) => {
    const id = req.params.id

    await categories.destroy({
        where: {
            id: id
        }
    })
    .then(result => {
        response_success(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}
