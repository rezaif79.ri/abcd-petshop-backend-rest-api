const { Op } = require("sequelize");
const {profiles, products, orders, order_histories} = require("../models")
const {
    response_success,
    response_internal_server_error,
} = require("../utils/response");

exports.index = async (req, res) => {
    try{
        const t_product = await products.count()
        const t_order = await orders.count()
        const t_history = await order_histories.count()
        const t_delivery = await orders.findAndCountAll({
            where: {
                status: "DELIVERY"
            }
        })
        const t_delivered = await orders.findAndCountAll({
            where: {
                status: "DELIVERED"
            }
        })
        const new_orders = await orders.findAll({
            include: ['address', 'products'],
            order: [
                ['id', 'DESC']
            ],
            where:{
                status: "PROCESS"
            }
        })

        return response_success(res, {
            count_products: t_product > 0 ? t_product : 0,
            count_orders: t_order > 0 ? t_order : 0,
            count_history: t_history > 0 ? t_history : 0,
            count_delivery: t_delivery.count > 0 ? t_delivery.count : 0,
            count_delivered: t_delivered.count > 0 ? t_delivered.count : 0,
            new_orders: new_orders
        });

    } catch (err) {
        // console.log(err)
        return response_internal_server_error(
            res,
            "Internal Server Error",
            err.message
        );
    }

}