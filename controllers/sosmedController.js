const {social_media} = require("../models")
const {response_success, response_internal_server_error, response_created} = require('../utils/response')

// return all data
exports.index = async (req, res) => {
    const page = req.query.page || 1;

    const offset = (page-1) * 20;

    const count = await social_media.count()

    await social_media.findAll({
        order: [
            ['id'],
        ],
        offset: offset,
        limit : 20
    })
    .then(result => {
        response_success(res, {count:count, social_media: result})
    })
    .catch(err => {
        // console.log(err)
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// post data
exports.store = async (req, res) => {
    const {name, link, icon} = req.body
    
    await social_media.create({
        name:name,
        link:link,
        icon:icon
    })
    .then(result => {
        response_created(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// edit specific data
exports.update = async (req, res) => {
    const id = req.params.id
    const {name, link, icon} = req.body
    
    await social_media.update({
        name:name,
        link:link,
        icon:icon
    },{
        where: {
            id: id
        },
        returning: true
    })
    .then(result => {
        response_success(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// delete certain data
exports.destroy = async (req, res) => {
    const id = req.params.id

    await social_media.destroy({
        where: {
            id:id
        }
    })
    .then(result => {
        response_success(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}