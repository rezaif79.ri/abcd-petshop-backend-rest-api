const { addresses } = require("../models");
const {
    response_success,
    response_internal_server_error,
    response_created,
    response_unauthorized
} = require("../utils/response");

const addressControllers = {
    async getAddresses(req, res) {
        try {            
            const { userId: user_id } = res.locals;
            
            const address = await addresses.findAll({
                where: { user_id },
            });

            return response_success(res, address);
        } catch (err) {
            return response_internal_server_error(
                res,
                "Internal Server Error",
                err.message
            );
        }
    },
    async createAddress(req, res) {
        try {
            const { userId: user_id } = res.locals;
            const { name, phone, province, city, postal_code, address, province_code, city_code } = req.body;

            const addAddress = await addresses.create({
                user_id,
                name,
                phone,
                province,
                city,
                postal_code,
                address,
                province_code,
                city_code,
            });

            return response_created(res, addAddress);
        } catch (err) {
            return response_internal_server_error(
                res,
                "Internal Server Error",
                err.message
            );
        }
    },
    async updateAddressById(req, res) {
        try {
            const { addressId } = req.params;
            const { userId: user_id } = res.locals;

            const {
                name,
                phone,
                province,
                city,
                postal_code,
                address,
                province_code,
                city_code,
            } = req.body;

            await addresses.update({
                name,
                phone,
                province,
                city,
                postal_code,
                address,
                province_code,
                city_code,
            }, {
                where: { id: addressId },
            });

            return response_created(res, {
                user_id,
                name,
                phone,
                province,
                city,
                postal_code,
                address,
                province_code,
                city_code,
            });
        } catch (err) {
            return response_internal_server_error(
                res,
                "Internal Server Error",
                err.message
            );
        }
    },
    async deleteAddressById(req, res) {
        try {
            const { addressId } = req.params;

            await addresses.destroy({
                where: {
                    id: addressId,
                },
            });
            console.log(addressId);

            return response_success(res, "Deleted");
        } catch (err) {
            return response_internal_server_error(
                res,
                "Internal Server Error",
                err.message
            );
        }
    },
};

module.exports = addressControllers;