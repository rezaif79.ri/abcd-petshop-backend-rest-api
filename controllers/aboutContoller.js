const {about_shop} = require("../models")
const {response_success, response_internal_server_error, response_created} = require('../utils/response')

// return all data
exports.index = async (req, res) => {

    await about_shop.findByPk(1)
    .then(result => {
        response_success(res, result)
    })
    .catch(err => {
        // console.log(err)
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// post data
exports.store = async (req, res) => {
    const {name, img, phone, email, description, maps, address, province,
    city, postal_code, province_code, city_code} = req.body

    await about_shop.create({
        name:name,
        img:img,
        phone:phone,
        email:email,
        description:description,
        maps:maps,
        address:address,
        province:province,
        city:city,
        postal_code:postal_code,
        province_code:province_code,
        city_code:city_code
    },{
        where: {
            id: id
        },
        returning: true
    })
    .then(result => {
        response_created(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// edit specific data
exports.update = async (req, res) => {
    const {name, img, phone, description, maps, address, province,
        city, postal_code, province_code, city_code} = req.body

    await about_shop.update({
        name:name,
        img:img,
        phone:phone,
        description:description,
        maps:maps,
        address:address,
        province:province,
        city:city,
        postal_code:postal_code,
        province_code:province_code,
        city_code:city_code
    },{
        where: {
            id: 1
        },
        returning: true
    })
    .then(result => {
        response_success(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}

// delete certain data
exports.destroy = async (req, res) => {
    const id = req.params.id

    await about_shop.destroy({
        where: {
            id:id
        }
    })
    .then(result => {
        response_success(res, result)
    })
    .catch(err => {
        response_internal_server_error(res, 'Internal Server Error', err)
    })
}