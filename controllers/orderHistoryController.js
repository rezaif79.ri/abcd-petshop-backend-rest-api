const {order_histories, product_order_histories} = require('../models');
const { Op } = require("sequelize");
const {
    response_success,
    response_internal_server_error,
    response_not_found
} = require("../utils/response");

const responses = (p, o) => {
    return {
        id: p.id,
        order_history_id: {
            id: o.id,
            user_name: o.user_name,
            user_phone: o.user_phone,
            user_province: o.user_province,
            user_city: o.user_city,
            user_postal_code: o.user_postal_code,
            user_address: o.user_address,
            grand_weight: o.grand_weight,
            grand_total: o.grand_total,
        },
        product_img: p.product_img,
        product_name: p.product_name,
        product_price: p.product_price,
        product_weight: p.product_weight,
        qty: p.qty,
        sub_weight: p.sub_weight,
        sub_total: p.sub_total

    }
}

module.exports = {
    showHistory: async(req,res) => {
        try {
            const page = req.query.page || 1;
            const offset = (page - 1) * 10;
            const title = !!req.query.title ? req.query.title : "";
            const checkStartDate = req.query.startDate || false
            const checkEndDate = req.query.endDate || false;
            const startDate = req.query.startDate || new Date().toISOString()
            const endDate = req.query.endDate || new Date().toISOString();
            let query = {
                user_name : {
                    [Op.iLike]: `%${title}%`
                }
            }
            console.log("ohoy "+startDate)

            if(!!checkStartDate || !!checkEndDate){
                query = {
                    user_name: {
                        [Op.iLike]: `%${title}%`
                    },
                    createdAt : {
                        [Op.gte]: startDate,
                        [Op.lte]: endDate 
                    }
                }
            }

            const count = await order_histories.findAndCountAll({
                where: query
            })

            const orderHistory = await order_histories.findAll({
                include: ['products'],
                where: query,
                offset,
                limit: 10
            })

            response_success(res, {count: count.count, orderHistory})
        } catch (err) {
            return response_internal_server_error(
                res,
                "Internal Server Error",
                err.message
            );
        }
    },
    showHistoryById: async (req,res) => {
        try {
            const {historyId: id} = req.params;
            const productHistory = await product_order_histories.findOne({where: { id }})
            
            if(productHistory == null || id != productHistory.id) return response_not_found(res, 'id product_order_histories not found!')
            
            const orderHistory = await order_histories.findOne({
                include:['products'],
                where: { id: productHistory.order_history_id }
            })
            
            // const data = responses(productHistory, orderHistory)

            response_success(res, orderHistory)
        } catch (err) {
            return response_internal_server_error(
                res,
                "Internal Server Error",
                err.message
            );
        }
    },
    deleteOrderHistory: async (req, res) => {
        try {
            const historyId = !!req.params.historyId ? req.params.historyId : false;
            const startDate = req.query.startDate;
            const endDate = req.query.endDate;
            
            if(historyId === false) {
                if(startDate == null || endDate == null) return response_success(res, 'please input start_date or end_date in query')
                
                await order_histories.destroy({
                    where: {
                        [Op.and]: 
                        [
                            { createdAt: { [Op.gte]: startDate } },
                            { createdAt: { [Op.lte]: endDate } }
                        ]
                    },
                })
                
                return response_success(res, "Success Deleted by date!")
            }
            
            const orderHistory = await product_order_histories.findByPk(historyId);
            
            if(orderHistory == null) return response_not_found(res, 'id product_order_histories not found!')
            
            await order_histories.destroy({
                where: {
                    id: historyId
                },
            })

            response_success(res, "Success Deleted by Id!")
        } catch (err) {
            response_internal_server_error( 
                res,
                "Internal Server Error",
                err.message)
        }
    }
}
