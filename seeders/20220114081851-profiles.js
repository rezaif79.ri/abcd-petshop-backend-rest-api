'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('profiles', [{
      user_id: '2bRTUZm4ZfeLWMHXEQ14pvilyT93',
      name: 'sandi',
      email: 'wijayakusumasandi@gmail.com',
      // password: 'adudu',
      phone: '12345',
      role_id: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      user_id: 'j4r8Y6S53EgsBSNj8tQcPfzON4G2',
      name: 'bobby',
      email: 'bobby@gmail.com',
      // password: 'boboiboy',
      phone: '12345',
      role_id: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    await queryInterface.bulkDelete('profiles', null, {});
  }
};
