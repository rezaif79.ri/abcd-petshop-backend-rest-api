'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('order_histories', [{
     user_name: 'udin',
     user_phone: '12345678',
     user_province: 'Sumatera Utara',
     user_city: 'Samosir',
     user_postal_code: '22392',
     user_address: 'jl.samosir no:9',
     grand_weight: 2.2,
     grand_total: 20000,
     createdAt: new Date(),
     updatedAt: new Date()
   }])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('order_histories', null, {})
  }
};
