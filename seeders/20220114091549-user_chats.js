'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('user_chats', [{
      sender_id: 2,
      recipient_id: 1,
      message: "hai",
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      sender_id: 1,
      recipient_id: 2,
      message: "hai juga",
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      sender_id: 2,
      recipient_id: 1,
      message: "piye kabare",
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      sender_id: 1,
      recipient_id: 2,
      message: "baik",
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      sender_id: 4,
      recipient_id: 3,
      message: "woi sok kenal ko ku tengok",
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      sender_id: 3,
      recipient_id: 4,
      message: "mata mu",
      createdAt: new Date(),
      updatedAt: new Date()
    }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('user_chats', null, {});

  }
};
