'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('animal_adoptions', [{
      name: 'kodomo',
      img: 'https://firebasestorage.googleapis.com/v0/b/binar-petshop.appspot.com/o/42850759048.jpg?alt=media&token=c74e1477-9bba-4205-823e-b90370ec6d97',
      age: 1,
      animal_race: 'reptil',
      categoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'bunglon',
      img: 'https://firebasestorage.googleapis.com/v0/b/binar-petshop.appspot.com/o/47503862164.jpg?alt=media&token=916800e2-5857-42d4-8220-bfb8a6cc8baa',
      age: 2,
      animal_race: 'reptil',
      categoryId: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'merak',
      img: 'https://firebasestorage.googleapis.com/v0/b/binar-petshop.appspot.com/o/47543667177.jpeg?alt=media&token=fe5fd23a-185f-40f9-9cb7-e38e1a5003cf',
      age: 3,
      animal_race: 'burung',
      categoryId: 4,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'pikachu',
      img: 'https://firebasestorage.googleapis.com/v0/b/binar-petshop.appspot.com/o/488377990.jpg?alt=media&token=dfe3d9b1-9843-4424-872a-a8a79cddd1ac',
      age: 1,
      animal_race: 'rare',
      categoryId: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'dinosaur',
      img: 'https://firebasestorage.googleapis.com/v0/b/binar-petshop.appspot.com/o/64781083534.jpg?alt=media&token=bdae65fd-528e-49dd-a552-b1b7e512bdd9',
      age: 10,
      animal_race: 'reptil',
      categoryId: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('animal_adoptions', null, {});

  }
};
