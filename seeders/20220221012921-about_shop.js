'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('about_shops', [{
     name: 'ABCD PETSHOP',
     phone: '0812021312',
     email: 'abcd-petshop@gmail.com',
     description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget urna eget diam mollis sagittis. Praesent sem dui, luctus ut hendrerit vel, blandit lacinia lectus. Mauris in scelerisque magna. Fusce porttitor quam in augue porttitor mollis. Fusce fringilla tempor nulla, eget posuere ex accumsan vel. In commodo semper ligula a lobortis. Aliquam a ligula eros. Integer scelerisque rutrum cursus. Mauris quis convallis libero, nec facilisis neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Fusce malesuada eget magna et ultricies. Nunc iaculis nisl sit amet turpis hendrerit accumsan. Integer eget mollis lorem.',
     maps: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d510166.39236795594!2d98.55575909725847!3d2.6114158906506373!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3031de07a843b6ad%3A0xc018edffa69c0d05!2sLake%20Toba!5e0!3m2!1sen!2sid!4v1645411061938!5m2!1sen!2sid',
     address: 'Samosir, North Sumatra',
     province: 'Sumatera Utara',
     city: 'Samosir',
     postal_code: '22392',
     province_code: '34',
     city_code: '389',
     createdAt: new Date(),
     updatedAt: new Date()
   }])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('about_shops', null, {})
  }
};
