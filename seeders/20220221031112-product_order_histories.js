'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('product_order_histories', [{
       order_history_id : 1,
       product_name: 'Kandang 2 tingkat',
       product_price: 10000,
       product_weight: 1.1,
       qty: 1,
       sub_weight: 1.1,
       sub_total: 10000,
       createdAt: new Date(),
       updatedAt: new Date()
    },{
      order_history_id : 1,
      product_name: 'maxxi',
      product_price: 10000,
      product_weight: 1.1,
      qty: 1,
      sub_weight: 1.1,
      sub_total: 10000,
      createdAt: new Date(),
      updatedAt: new Date()
   }])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('product_order_histories', null, {});
  }
};
