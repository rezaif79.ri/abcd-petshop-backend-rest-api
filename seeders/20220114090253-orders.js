'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        await queryInterface.bulkInsert('orders', [{
                user_id: "2bRTUZm4ZfeLWMHXEQ14pvilyT93",
                address_id: 2,
                shipping_costs: 45000,
                grand_total: 789000,
                grand_weight: 5,
                status: 'DELIVERED',
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                user_id: "j4r8Y6S53EgsBSNj8tQcPfzON4G2",
                address_id: 4,
                shipping_costs: 50000,
                grand_total: 70000,
                grand_weight: 6,
                status: 'PROCESS',
                createdAt: new Date(),
                updatedAt: new Date()
            }
        ], {});
    },

    down: async(queryInterface, Sequelize) => {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        await queryInterface.bulkDelete('orders', null, {});
    }
};