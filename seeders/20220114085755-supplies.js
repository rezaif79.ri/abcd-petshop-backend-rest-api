'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('supplies', [{
        product_id:1,
        qty: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        product_id:1,
        qty: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        product_id:1,
        qty: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        product_id:1,
        qty: 10,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        product_id:2,
        qty: 10,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        product_id:3,
        qty: 20,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        product_id:4,
        qty: 15,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        product_id:5,
        qty: 9,
        createdAt: new Date(),
        updatedAt: new Date()
      }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('supplies', null, {});
  }
};
