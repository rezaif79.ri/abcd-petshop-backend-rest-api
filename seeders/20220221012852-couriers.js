'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('couriers', [{
      name: 'JNE',
      courier_code: 'jne',
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      name: 'POS Indonesia',
      courier_code: 'pos',
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      name: 'TIKI',
      courier_code: 'tiki',
      createdAt: new Date(),
      updatedAt: new Date()
    }])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('couriers', null, {});
  }
};
