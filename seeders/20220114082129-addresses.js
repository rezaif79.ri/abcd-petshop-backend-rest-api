'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('addresses', [{
      user_id: '2bRTUZm4ZfeLWMHXEQ14pvilyT93',
      name: 'adudu',
      phone: '123456',
      province: 'Gorontalo',
      city: 'Gorontalo Utara',
      postal_code: '96611',
      address: 'jl. bersama dia',
      province_code: '7',
      city_code: '131',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      user_id: '2bRTUZm4ZfeLWMHXEQ14pvilyT93',
      name: 'adudu2',
      phone: '123456',
      province: 'Sulawesi Selatan',
      city: 'Enrekang',
      postal_code: '91719',
      address: 'jl. bersama dia dan dia',
      province_code: '23',
      city_code: '123',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      user_id: '2bRTUZm4ZfeLWMHXEQ14pvilyT93',
      name: 'boboiboy',
      phone: '123456',
      province: 'Sulawesi Selatan',
      city: 'Gowa',
      postal_code: '92111',
      address: 'jl. bersama-sama',
      province_code: '28',
      city_code: '132',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      user_id: 'j4r8Y6S53EgsBSNj8tQcPfzON4G2',
      name: 'upin',
      phone: '123456',
      province: 'Nanggroe Aceh Darussalam (NAD)',
      city: 'Aceh Barat Daya',
      postal_code: '23764',
      address: 'jl. bersama ipin',
      province_code: '21',
      city_code: '2',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      user_id: 'j4r8Y6S53EgsBSNj8tQcPfzON4G2',
      name: 'adudu3',
      phone: '123456',
      province: 'Jawa Tengah',
      city: 'Demak',
      postal_code: '59519',
      address: 'jl. bersama dia dan dia dan dia',
      province_code: '10',
      city_code: '113',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('addresses', null, {});
  }
};
