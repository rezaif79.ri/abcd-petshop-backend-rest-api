'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        await queryInterface.bulkInsert('shopping_carts', [{
                user_id: '2bRTUZm4ZfeLWMHXEQ14pvilyT93',
                product_id: 1,
                quantity: 3,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                user_id: 'Ut4tu9VoX9RPa2xCevzrVf8uzGW2',
                product_id: 2,
                quantity: 2,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                user_id: '2bRTUZm4ZfeLWMHXEQ14pvilyT93',
                product_id: 4,
                quantity: 2,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                user_id: '2bRTUZm4ZfeLWMHXEQ14pvilyT93',
                product_id: 5,
                quantity: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            // {
            //     user_id: '2bRTUZm4ZfeLWMHXEQ14pvilyT93',
            //     product_id: 7,
            //     quantity: 1,
            //     createdAt: new Date(),
            //     updatedAt: new Date()
            // },
            {
                user_id: 'Ut4tu9VoX9RPa2xCevzrVf8uzGW2',
                product_id: 2,
                quantity: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                user_id: 'Ut4tu9VoX9RPa2xCevzrVf8uzGW2',
                product_id: 4,
                quantity: 1,
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                user_id: 'Ut4tu9VoX9RPa2xCevzrVf8uzGW2',
                product_id: 5,
                quantity: 3,
                createdAt: new Date(),
                updatedAt: new Date()
            }
        ], {});
    },

    down: async(queryInterface, Sequelize) => {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        await queryInterface.bulkDelete('shopping_carts', null, {});
    }
};