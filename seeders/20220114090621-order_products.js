'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('order_products', [{
      product_id: 1,
      order_id: 1,
      qty: 5,
      sub_weight: 5,
      sub_total: 5000,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      product_id: 5,
      order_id: 1,
      qty: 6,
      sub_weight: 6,
      sub_total: 6000,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      product_id: 2,
      order_id: 1,
      qty: 10,
      sub_weight: 10.3,
      sub_total: 15000,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      product_id: 3,
      order_id: 1,
      qty: 5,
      sub_weight: 5.5,
      sub_total: 7000,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      product_id: 2,
      order_id: 2,
      qty: 15,
      sub_weight: 15.5,
      sub_total: 15000,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      product_id: 1,
      order_id: 2,
      qty: 10,
      sub_weight: 9.8,
      sub_total: 40000,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('order_products', null, {});

  }
};
