'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('categories', [{
      name: 'reptil',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'mamalia',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'purba',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'eksotis',
      createdAt: new Date(),
      updatedAt: new Date()
    }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('categories', null, {});

  }
};
