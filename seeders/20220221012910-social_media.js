'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('social_media', [{
      name: "Instagram",
      link: "https://www.instagram.com/indozonetech/",
      icon: "FiInstagram",
      createdAt: new Date(),
      updatedAt: new Date()
   },{
      name: "Facebook",
      link: "https://www.facebook.com/TimnasIndonesiaHebat/",
      icon: "ImFacebook",
      createdAt: new Date(),
      updatedAt: new Date()
    }])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('social_media', null, {});
  }
};
