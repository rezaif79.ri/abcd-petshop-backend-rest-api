'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('products', [{
       name: 'kandang 1 tingkat',
       description: 'paten',
       qty: 20,
       price: 1000,
       img: 'https://firebasestorage.googleapis.com/v0/b/binar-petshop.appspot.com/o/WhatsApp%20Image%202022-01-24%20at%2009.08.30.jpeg?alt=media&token=120f0ea3-597e-4f5b-aa2c-e9bbea9d1b1d',
       weight: 1.1,
       sold:2,
       createdAt: new Date(),
       updatedAt: new Date()
      },
      {
        name: 'kandang 2 tingkat',
        description: 'paten2',
        qty: 10,
        price: 2000,
        img: 'https://firebasestorage.googleapis.com/v0/b/binar-petshop.appspot.com/o/WhatsApp%20Image%202022-01-24%20at%2009.08.30%20(1).jpeg?alt=media&token=708d4b3d-44e0-4fa0-8265-f41c0736afd7',
        weight: 2.1,
        sold:0,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'kandang 4 tingkat',
        description: 'paten3',
        qty: 15,
        price: 555000,
        img: 'https://firebasestorage.googleapis.com/v0/b/binar-petshop.appspot.com/o/WhatsApp%20Image%202022-01-24%20at%2009.08.29.jpeg?alt=media&token=89bc51e6-12e4-4584-ad94-54d856498a60',
        weight: 4,
        sold:1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'bola',
        description: 'paten paten',
        qty: 50,
        price: 1500,
        img: 'https://firebasestorage.googleapis.com/v0/b/binar-petshop.appspot.com/o/WhatsApp%20Image%202022-01-24%20at%2009.08.29%20(1).jpeg?alt=media&token=50ffdb25-416c-447b-b730-10fba579674c',
        weight: 0.8,
        sold:6,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'vitamin c',
        description: 'oke la',
        qty: 20,
        price: 25000,
        img: 'https://firebasestorage.googleapis.com/v0/b/binar-petshop.appspot.com/o/WhatsApp%20Image%202022-01-24%20at%2009.08.28.jpeg?alt=media&token=e2df4196-4bf0-454c-9941-841d765a8947',
        weight: 0.6,
        sold:8,
        createdAt: new Date(),
        updatedAt: new Date()
      }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('products', null, {});

  }
};
