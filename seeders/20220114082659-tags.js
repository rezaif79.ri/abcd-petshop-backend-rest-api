'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('tags', [{
      name: 'kandang',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'cat food',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'dog food',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'toys',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'vitamin',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('tags', null, {});

  }
};
