# ABCD PetShop - Backend API

This project serves API needed for the frontend app, backend designed to work with Firebase and Midtrans. Using Firebase in order to save and get user account registered in this app and Midtrans for payment gateway in app transaction.

The server itself is implemented in [NodeJs](https://nodejs.org/en/) using [ExpressJs](https://expressjs.com/). 

The server logs all API requests it performs to the terminal using morgan logger, so you can see request traffic coming from the frontend app.

## Integration with the ABCD PetShop App on Backend API
This backend API routes are well planned and documented using Swagger OpenAPI. You can read more about the API [here](https://app.swaggerhub.com/apis/bddd-bags/petshop/1.0.0#/).

## Preparation
If you wish to run the server, the first step is [installing NodeJs](https://nodejs.org/en/download/) or using [package manager](https://nodejs.org/en/download/package-manager/).

## Configure project env
After that, configure the project environment variables in .env files. You will need to configure database config, firebase and midtrans variable using your own key.

## Running the server
Once that's out of the way, open a terminal and run the following command:

```
npm install
```

And, install nodemon globally using package manager for development stage:

```
npm install -g nodemon
```

which will install all the packages required to run the server, the npm or node package manager will installing the project's dependencies. 

(For production!) Still in the terminal, navigate to the project's directory and run:

```
npm run build
```

(For development!) Still in the terminal, navigate to the project's directory and run:

```
npm run develop
```

which should result in output such as:

```
> nodemon index.js

[nodemon] 2.0.15
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node index.js`
listening on 8000
GET /products 302 278 - 326.952 ms
GET /products?title=&min_price=0&max_price=9999999999&tags=&limit=10&page=1&best=0&redirect=0 500 83 - 121.370 ms
```

indicating the server is now listening at port 8000.


## License
The sample backend is available under the Apache 2.0 license. See the [LICENSE](./LICENSE) file for more info.