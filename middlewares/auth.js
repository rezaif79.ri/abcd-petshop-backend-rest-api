const admin = require("../config/firebaseConfig");
const { response_internal_server_error, response_unauthorized } = require("../utils/response");
const { profiles } = require("../models");

const verifyToken =
    async(req, res, next) => {

        try {
            const rawToken = req.headers.authorization;

            const decodedValue = await admin.auth().verifyIdToken(rawToken);
            if (decodedValue) {
                res.locals.userId = decodedValue.user_id;
                res.locals.email = decodedValue.email;
                res.locals.name = decodedValue.name;
                return next();
            }
            return response_unauthorized(res, err.message);
        } catch (err) {
            return response_internal_server_error(res, err.message);
        }
    }


const verifyTokenAndRole = (roles) => {
    return async(req, res, next) => {

        try {
            const rawToken = req.headers.authorization;
            const decodedValue = await admin.auth().verifyIdToken(rawToken);
            if (decodedValue) {
                res.locals.userId = decodedValue.user_id;
                res.locals.email = decodedValue.email;
                res.locals.name = decodedValue.name;
                const user = await profiles.findOne({ where: { email: decodedValue.email }, include: ['role'] });
                if (!user) {
                    return response_unauthorized(res, `User Profile Undefine`);
                }
                if (roles[0] !== user.role.dataValues.name) {
                    return response_unauthorized(res, `User should be one of this specified role ${roles}`);
                }
                res.locals.role = user.role.name;
                return next();
            }
            return response_unauthorized(res, err.message);
        } catch (err) {
            res.status(201).json({
                data: err,
                message: "Unauthorize User"
            })
        }
    }
}


module.exports = { verifyToken, verifyTokenAndRole };