require("dotenv").config();
const config = require("./midtransConfig.json");
const { Buffer } = require("buffer");
const https = require("https");
const midtransClient = require('midtrans-client');
// Create Snap API instance
const snap = new midtransClient.Snap({
    // Set to true if you want Production Environment (accept real transaction).
    isProduction: false,
    serverKey: config.MIDTRANS_SERVER_KEY
});

const validateSignature = async(order_id) => {

    return new Promise((resolve, reject) => {
        const rawKey = config.MIDTRANS_SERVER_KEY;
        console.log(rawKey);
        const serverKey = Buffer.from(rawKey).toString('base64');
        const options = {
            method: 'GET',
            host: `api.sandbox.midtrans.com`,
            path: `/v2/${order_id}/status`,
            headers: { Authorization: `Basic ${serverKey}` },
        };
        const req = https.request(options, (res) => {
            if (res.statusCode < 200 || res.statusCode >= 300) {
                return reject(new Error('statusCode=' + res.statusCode));
            }
            var body = [];
            res.on('data', function(chunk) {
                body.push(chunk);
            });
            res.on('end', function() {
                try {
                    body = JSON.parse(Buffer.concat(body).toString());
                } catch (e) {
                    reject(e);
                }
                resolve(body);
            });
        });
        req.on('error', (e) => {
            reject(e.message);
        });
        // send the request
        req.end();
    });

}

function generateOrderId(length) {

    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    let dateTimeStamps = new Date().toISOString().split("T")[0];
    dateTimeStamps = dateTimeStamps.replace(/\-/g, '');
    return `${dateTimeStamps}${result}`;


}
module.exports = { snap, validateSignature, generateOrderId }