const express = require("express");
const { getUserShoppingCartController, addToShoppingCartController, deleteShoppingCartController, updateCartQuantityController } = require("../controllers/cartController");
const { verifyToken } = require("../middlewares/auth");
const cartRouter = express.Router();

cartRouter.get("/", verifyToken, getUserShoppingCartController);
cartRouter.put("/", verifyToken, updateCartQuantityController);
cartRouter.post("/", verifyToken, addToShoppingCartController);
cartRouter.delete("/:cart_id", verifyToken, deleteShoppingCartController);

module.exports = cartRouter;