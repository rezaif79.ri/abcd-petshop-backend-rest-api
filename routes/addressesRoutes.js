const express = require("express");
const addressControllers = require("../controllers/addressController");
const { verifyToken, verifyTokenAndRole } = require("../middlewares/auth");
const router = express.Router();

router.get("/v1/user/address", verifyToken, addressControllers.getAddresses);
router.post("/v1/user/address", verifyToken, addressControllers.createAddress);
router.put("/v1/user/address/:addressId", verifyToken, addressControllers.updateAddressById);
router.delete(
    "/v1/user/address/:addressId", verifyToken, addressControllers.deleteAddressById
);

module.exports = router;