const express = require("express");
const router = express.Router();
const { verifyToken, verifyTokenAndRole } = require("../middlewares/auth");

const dashboardController = require('../controllers/dashboardController')

router.get("/admin/v1/dashboard", verifyTokenAndRole(['ADMIN']), dashboardController.index)

module.exports = router