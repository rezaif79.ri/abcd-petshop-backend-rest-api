const express = require("express");
const { registerController, checkRoleController } = require("../controllers/authController");
const { verifyToken, verifyTokenAndRole } = require("../middlewares/auth");
const authRouter = express.Router();

authRouter.post("/register", verifyToken, registerController);
authRouter.get("/checkAdmin", verifyTokenAndRole(["ADMIN"]), checkRoleController);
module.exports = authRouter;