const express = require("express");
const orderRouter = express.Router();
const {
    initOrderController,
    cancelOrderController,
    createOrderController,
    midtransNotificationController,
    statusOrder,
    getOrders,
    getOrderById,
    updateResi,
    deleteOrder,
    getUserOrdersController,
    getOrderDetailsController
} = require("../controllers/orderController");
const { verifyToken, verifyTokenAndRole } = require("../middlewares/auth");


orderRouter.post("/v1/user/order/init", verifyToken, initOrderController);
orderRouter.post("/v1/user/order/cancel/:orderId", verifyToken, cancelOrderController);
orderRouter.post("/v1/user/order/create", verifyToken, createOrderController);
orderRouter.post("/v1/user/order/midtrans-notification", midtransNotificationController);
// orderRouter.put("/v1/user/order/:id", verifyToken, statusOrder);
orderRouter.get("/v1/user/order/", verifyToken, getUserOrdersController);
orderRouter.get("/v1/user/order/:id", verifyToken, getOrderDetailsController);
orderRouter.get("/admin/v1/order", verifyTokenAndRole(['ADMIN']), getOrders);
orderRouter.get("/admin/v1/order/:orderId", verifyTokenAndRole(['ADMIN']), getOrderById);
orderRouter.put("/admin/v1/order/:orderId", verifyTokenAndRole(['ADMIN']), updateResi);
orderRouter.delete("/admin/v1/order/:orderId", verifyTokenAndRole(['ADMIN']), deleteOrder);

module.exports = {
    orderRouter
}