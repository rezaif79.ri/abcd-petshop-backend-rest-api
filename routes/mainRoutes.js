const express = require("express");
const mainRouter = express.Router();

mainRouter.get("/test", (req, res) => {
    return res.status(200).json({
        message: "This is a test route",
        data: {},
        errors: ""
    })
});

module.exports = mainRouter;