const express = require("express");
const router = express.Router();
const { verifyToken, verifyTokenAndRole } = require("../middlewares/auth");
const productController = require('../controllers/productController')

router.get('/products', productController.getAllProduct)
router.get('/products/:productId', productController.getProductById)

router.get('/admin/v1/products', verifyTokenAndRole(["ADMIN"]), productController.getAllProduct)
router.post('/admin/v1/products', verifyTokenAndRole(["ADMIN"]), productController.postNewProduct)
router.get('/admin/v1/products/:productId', verifyTokenAndRole(["ADMIN"]), productController.getProductById)
router.put('/admin/v1/products/:productId', verifyTokenAndRole(["ADMIN"]), productController.updateProductById)
router.delete('/admin/v1/products/:productId', verifyTokenAndRole(["ADMIN"]), productController.deleteProductById)

module.exports = router