const express = require("express");
const router = express.Router();
const { verifyToken, verifyTokenAndRole } = require("../middlewares/auth");

const sosmedController = require("../controllers/sosmedController")

router.get("/sosmed", sosmedController.index)
router.post("/sosmed", verifyTokenAndRole(["ADMIN"]), sosmedController.store)
router.put("/sosmed/:id", verifyTokenAndRole(["ADMIN"]), sosmedController.update)
router.delete("/sosmed/:id", verifyTokenAndRole(["ADMIN"]), sosmedController.destroy)

module.exports = router