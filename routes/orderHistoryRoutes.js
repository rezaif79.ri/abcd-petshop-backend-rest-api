const express = require("express");
const orderHistoryController = require("../controllers/orderHistoryController");
const { verifyToken, verifyTokenAndRole } = require("../middlewares/auth");
const router = express.Router();

router.get("/admin/v1/histories", verifyTokenAndRole(['ADMIN']), orderHistoryController.showHistory);
router.get("/admin/v1/histories/:historyId", verifyTokenAndRole(['ADMIN']), orderHistoryController.showHistoryById);
router.delete("/admin/v1/histories/:historyId?", verifyTokenAndRole(['ADMIN']), orderHistoryController.deleteOrderHistory)

module.exports = router;