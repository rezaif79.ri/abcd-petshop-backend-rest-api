const express = require("express");
const router = express.Router();
const { verifyToken, verifyTokenAndRole } = require("../middlewares/auth");

const aboutController = require("../controllers/aboutContoller")

router.get("/about", aboutController.index)
router.put("/admin/v1/about", verifyTokenAndRole(["ADMIN"]), aboutController.update)

module.exports = router