const express = require("express");
const router = express.Router();

const deliveryController = require("../controllers/deliveryController")

router.get("/province", deliveryController.province)
router.get("/city", deliveryController.city)
router.post("/cost", deliveryController.cost)

module.exports = router