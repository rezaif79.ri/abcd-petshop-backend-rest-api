const express = require("express");
const router = express.Router();
const { verifyTokenAndRole } = require("../middlewares/auth");

const adoptController = require('../controllers/adoptController')

router.get('/adopt', adoptController.index)
// router.get('/adopt/:id', adoptController.show)

router.get('/admin/v1/adopt', verifyTokenAndRole(["ADMIN"]), adoptController.index)
router.post('/admin/v1/adopt', verifyTokenAndRole(["ADMIN"]), adoptController.store)
router.get('/admin/v1/adopt/:id', verifyTokenAndRole(["ADMIN"]), adoptController.show)
router.put('/admin/v1/adopt/:id', verifyTokenAndRole(["ADMIN"]), adoptController.update)
router.delete('/admin/v1/adopt/:id', verifyTokenAndRole(["ADMIN"]), adoptController.destroy)

module.exports = router