const express = require("express");
const router = express.Router();
const { verifyToken, verifyTokenAndRole } = require("../middlewares/auth");

const categoryController = require('../controllers/categoriesController');
const { response_unauthorized } = require("../utils/response");

router.get('/categories', categoryController.index)
router.get('/admin/v1/categories/:id', verifyTokenAndRole(["ADMIN"]), categoryController.show)
router.post('/admin/v1/categories', verifyTokenAndRole(["ADMIN"]), categoryController.store)
router.put('/admin/v1/categories/:id', verifyTokenAndRole(["ADMIN"]),  categoryController.update)
router.delete('/admin/v1/categories/:id', verifyTokenAndRole(["ADMIN"]),  categoryController.destroy)

module.exports = router