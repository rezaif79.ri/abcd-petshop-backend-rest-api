const mainRouter = require("./mainRoutes");
const categoriesRouter = require("./categoriesRoutes");
const adoptRouter = require("./adoptRoutes");
const productRouter = require("./productRoutes");
const tagsRouter = require("./tagsRoutes");
const authRouter = require("./authRoutes");
const deliveryRouter = require("./deliveryRoutes");
const cartRouter = require("./cartRoutes");
const addressesRouter = require("./addressesRoutes");
const suppliesRouter = require("./suppliesRoutes");
const { orderRouter } = require("./orderRoutes");
const courierRouter = require("./courierRoutes");
const sosmedRouter = require("./sosmedRoutes");
const aboutRouter = require("./aboutRoutes");
const orderHistoryRouter = require("./orderHistoryRoutes")
const dashboardRouter = require("./dashboardRoutes")

function routes(app) {
    //Templates for using routes file
    app.use("/mainRoutes", mainRouter);
    app.use(categoriesRouter);
    app.use(adoptRouter);
    app.use(productRouter);
    app.use(tagsRouter);
    app.use("/auth", authRouter);
    app.use(deliveryRouter);
    app.use("/v1/user/cart", cartRouter);
    app.use(orderRouter);
    app.use(addressesRouter);
    app.use(suppliesRouter);
    app.use(courierRouter);
    app.use(sosmedRouter);
    app.use(aboutRouter);
    app.use(orderHistoryRouter);
    app.use(dashboardRouter);
    
    //Routes without specified router file defined here
    app.all("*", (req, res) => {
        return res.status(404).json({
            message: "Not Found",
            data: {},
            errors: {},
        });
    });
}

module.exports = routes;