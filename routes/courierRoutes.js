const express = require("express");
const router = express.Router();
const { verifyToken, verifyTokenAndRole } = require("../middlewares/auth");

const courierController = require("../controllers/courierController")

router.get("/couriers", courierController.index)
router.get("/admin/v1/courier/:id", verifyTokenAndRole(["ADMIN"]),  courierController.show)
router.post("/admin/v1/courier", verifyTokenAndRole(["ADMIN"]),  courierController.store)
router.put("/admin/v1/courier/:id", verifyTokenAndRole(["ADMIN"]), courierController.update)
router.delete("/admin/v1/courier/:id", verifyTokenAndRole(["ADMIN"]), courierController.destroy)

module.exports = router