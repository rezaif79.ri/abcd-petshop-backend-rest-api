const express = require("express");
const router = express.Router();

const tagsController = require('../controllers/tagsController')
const { verifyTokenAndRole } = require("../middlewares/auth");

router.get('/tags', tagsController.getAllTags)
router.get('/admin/v1/tags', verifyTokenAndRole(["ADMIN"]), tagsController.getAllTags)
router.get('/admin/v1/tags/:tagsId', verifyTokenAndRole(["ADMIN"]), tagsController.showTagsById)
router.post('/admin/v1/tags', verifyTokenAndRole(["ADMIN"]), tagsController.postNewTags)
router.put('/admin/v1/tags/:tagsId', verifyTokenAndRole(["ADMIN"]), tagsController.updateTagsById)
router.delete('/admin/v1/tags/:tagsId', verifyTokenAndRole(["ADMIN"]), tagsController.deleteTagsById)

module.exports = router