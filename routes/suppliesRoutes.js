const express = require("express");
const router = express.Router();
const { verifyToken, verifyTokenAndRole } = require("../middlewares/auth");

const suppliesController = require("../controllers/suppliesController")

router.get("/admin/v1/supplies", verifyTokenAndRole(['ADMIN']), suppliesController.index)
router.post("/admin/v1/supplies", verifyTokenAndRole(['ADMIN']), suppliesController.store)
router.get("/admin/v1/supplies/:id", verifyTokenAndRole(['ADMIN']), suppliesController.show)
router.put("/admin/v1/supplies/:id", verifyTokenAndRole(['ADMIN']), suppliesController.update)
router.delete("/admin/v1/supplies/:id", verifyTokenAndRole(['ADMIN']), suppliesController.destroy)

module.exports = router