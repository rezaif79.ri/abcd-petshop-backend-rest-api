const { shopping_carts, products } = require("../models")

const addToCartService = async(cartData) => {
    try {
        const { userId: user_id, product_id, quantity } = cartData;

        const countExist = await shopping_carts.count({
            where: {
                user_id,
                product_id
            }
        });

        if (countExist != 0) {
            const existedCart = await shopping_carts.findOne({
                where: {
                    user_id,
                    product_id
                }
            });

            existedCart.quantity = existedCart.quantity + quantity;
            existedCart.save();
            return { error: {}, status: true, data: existedCart };
        } else {
            const newCart = await shopping_carts.create({
                user_id: user_id,
                product_id: product_id,
                quantity: quantity
            });
            return { error: {}, status: true, data: newCart };
        }
    } catch (err) {
        console.log(err);
        return { error: { message: err.messsage }, status: false, data: {} }
    }
}

const getUserShoppingCartService = async(user_id) => {
    try {
        const carts = await shopping_carts.findAll({
            where: {
                user_id
            },
            order: [
                ['id', 'DESC']
            ],
            include: "product"
        });

        let totalPrice = 0;
        let totalWeight = 0;
        carts.forEach((cart) => {
            totalPrice += cart.quantity * cart.product.price;
            totalWeight += cart.product.weight;
        });

        return { error: {}, status: true, data: { carts, totalPrice, totalWeight } };
    } catch (err) {
        return { error: { message: err.message }, status: false, data: {} }
    }
}

const deleteShoppingCartService = async(cart_id, user_id) => {
    try {
        const cartItem = await shopping_carts.findByPk(cart_id);
        if (cartItem.user_id != user_id) {
            throw new Error("Unauthorized");
        }
        await cartItem.destroy();
        return { error: {}, status: true, data: {} };
    } catch (err) {
        return { error: { message: err.message }, status: false, data: {} }
    }
}

const updateCartQuantityService = async(cart_id, user_id, method) => {
    try {
        const cartItem = await shopping_carts.findByPk(cart_id);
        const product = await products.findByPk(cartItem.product_id);
        if (cartItem.user_id != user_id) {
            throw new Error("Unauthorized");
        }
        if (method == "SUBSTRACT") {
            cartItem.quantity = cartItem.quantity - 1;
        } else if (method == "ADD") {
            if(product.qty > 0) cartItem.quantity = cartItem.quantity + 1;
            else throw "Product stock quantity are 0 (empty product)";
        } else {

        }
        cartItem.save();
        return { error: {}, status: true, data: cartItem }
    } catch (err) {
        return { error: { message: err.message }, status: false, data: {} }
    }
}

module.exports = {
    addToCartService,
    getUserShoppingCartService,
    deleteShoppingCartService,
    updateCartQuantityService
}