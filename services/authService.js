const { profiles, roles } = require("../models");

const loginService = async(userData) => {
    try {
        const { userId, email, name } = userData;
        const role = await roles.findOne({ where: { name: 'CUSTOMER' } });
        const user = await profiles.create({ user_id: userId, email: email, name: name, role_id: role.id });
        return { status: true, err: {}, data: { user } };
    } catch (err) {
        return { status: false, err: { message: "Could not register user" }, data: {} };
    }
}

module.exports = { loginService };