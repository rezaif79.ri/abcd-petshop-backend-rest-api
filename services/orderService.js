const { shopping_carts, products, order_products, orders, order_histories, product_order_histories, addresses } = require("../models");
const { snap, generateOrderId, validateSignature } = require("../utils/midtrans");
const initialOrderService = async(user_id) => {
    try {
        const carts = await shopping_carts.findAll({
            where: {
                user_id
            },
            include: "product"
        });

        const updateQuantityPromises = [];
        carts.forEach((cart) => {
            updateQuantityPromises.push(
                products.update({
                    qty: cart.product.qty - cart.quantity
                }, {
                    where: {
                        id: cart.product.id
                    }
                })
            )
        });

        await Promise.all(updateQuantityPromises);

        return { status: true, data: {}, error: {} };
    } catch (err) {
        return { status: false, data: {}, error: { message: err.message } }
    }
}

const cancelOrderService = async(user_id, orderId) => {
    try {
        const userOrder = await orders.findOne({
            where: {
                user_id,
                id: orderId
            },
            include: "products"
        });

        const updateQuantityPromises = [];
        const orderProducts = await order_products.findAll({ where: { order_id: userOrder.id }, include: "product" });
        orderProducts.forEach((orderProduct) => {
            updateQuantityPromises.push(
                products.update({
                    qty: orderProduct.product.qty + orderProduct.qty
                }, {
                    where: {
                        id: orderProduct.product.id
                    }
                })
            )
        });

        await Promise.all(updateQuantityPromises);
        userOrder.status = 'CANCEL';
        userOrder.save();
        return { status: true, data: {}, error: {} };
    } catch (err) {
        return { status: false, data: {}, error: { message: err.message } }
    }
}

const createOrderService = async(orderData) => {
    try {
        const { user_id, address_id, shipping_costs, email, name } = orderData;

        const [carts, order, address] = await Promise.all([
            shopping_carts.findAll({
                where: {
                    user_id: user_id
                },
                include: "product"
            }),
            orders.create({
                user_id,
                address_id,
                shipping_costs,
                grand_total: 0,
                grand_weight: 0,
                status: 'PENDING'
            }),
            addresses.findOne({ where: { id: address_id } })
        ]);

        const orderHistory = await order_histories.create({
            user_name: address.name,
            user_phone: address.phone,
            user_province: address.province,
            user_city: address.city,
            user_postal_code: address.postal_code,
            user_address: address.address,
            grand_weight: order.grand_weight,
            grand_total: order.grand_total,
        })

        const orderProductsAndHistoriesPromises = [];
        let grand_total = 0;
        let grand_weight = 0;
        carts.forEach((cart) => {

            const sub_total = cart.quantity * cart.product.price;
            const sub_weight = cart.quantity * cart.product.weight;

            orderProductsAndHistoriesPromises.push(
                order_products.create({
                    product_id: cart.product_id,
                    order_id: order.id,
                    qty: cart.quantity,
                    sub_total,
                    sub_weight
                })
            );
            grand_total += sub_total;
            grand_weight += sub_weight;

            orderProductsAndHistoriesPromises.push(
                product_order_histories.create({
                    order_history_id: orderHistory.id,
                    product_img: cart.product.img,
                    product_name: cart.product.name,
                    product_price: cart.product.price,
                    product_weight: cart.product.weight,
                    qty: cart.quantity,
                    sub_weight,
                    sub_total
                })
            )
        });

        grand_total += shipping_costs;
        order.grand_total = grand_total;
        order.grand_weight = grand_weight;
        order.midtrans_order_id = generateOrderId(24);
        console.log("Grand total : " + grand_total);
        let parameter = {
            "transaction_details": {
                "order_id": order.midtrans_order_id,
                "gross_amount": grand_total
            },
            "credit_card": {
                "secure": true
            },
            "customer_details": {
                "first_name": name.split(" ")[0] || " ",
                "last_name": name.split(" ")[1] || " ",
                "email": email,
            },
            "callbacks": {
                "finish": `https://abcd-petshop.herokuapp.com/order/${order.id}`
            }
        };
        const midtransData = await snap.createTransaction(parameter);
        order.midtrans_snap_token = midtransData.token;
        await Promise.all(orderProductsAndHistoriesPromises);

        await order.save();

        await shopping_carts.destroy({
            where: {
                user_id
            }
        });
        return { data: { order, snap_token: midtransData.token }, error: {}, status: true };
    } catch (err) {
        console.log(err);
        return { error: { message: err.message }, status: false, data: {} }
    }
}


const getUserOrdersService = async(user_id) => {
    try {
        const userOrders = await orders.findAll({
            where: {
                user_id
            },
            include: ["products", "address"]
        });
        return { error: {}, status: true, data: userOrders }
    } catch (err) {
        return { error: { message: err.message }, status: false, data: {} }
    }
}

const getOrderDetailsService = async(order_id, user_id) => {
    try {

        const order = await orders.findOne({
            where: {
                id: order_id,
                user_id: user_id
            },
            include: ["products", "address"]
        });
        let paymentStatus = "SUCCESS";
        if (order.midtrans_order_id !== "") {
            const paymentStatusResponse = await validateSignature(order.midtrans_order_id);

            const paymentStatusMidtrans = paymentStatusResponse.transaction_status;


            if (paymentStatusMidtrans == 'capture' || paymentStatusMidtrans == 'settlement') {
                paymentStatus = 'SUCCESS';
            } else if (paymentStatusMidtrans == 'cancel' ||
                paymentStatusMidtrans == 'deny' ||
                paymentStatusMidtrans == 'expire') {
                paymentStatus = 'CANCEL';
            } else {
                if (order.status == 'CANCEL') {
                    paymentStatus == 'CANCEL';
                } else {
                    paymentStatus = 'NO PAYMENT';
                }
            }
        }
        return { error: {}, status: true, data: { order, paymentStatus } };
    } catch (err) {
        return { error: { message: err.message }, status: false, data: {} }
    }
}

module.exports = {
    initialOrderService,
    cancelOrderService,
    createOrderService,
    getUserOrdersService,
    getOrderDetailsService
}