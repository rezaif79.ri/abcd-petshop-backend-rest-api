const { orders, products, order_products } = require("../models");

const { validateSignature } = require("../utils/midtrans");

const receiveMidtransNotification = async(order_id) => {
    try {
        const validateResponse = await validateSignature(order_id);

        let orderId = validateResponse.order_id;
        let transactionStatus = validateResponse.transaction_status;
        let fraudStatus = validateResponse.fraud_status;
        const order = await orders.findOne({
            where: {
                midtrans_order_id: orderId
            },
            include: "products"
        });


        if (transactionStatus == 'capture') {
            if (fraudStatus == 'challenge') {
                order.status = 'CHALLENGE';
            } else if (fraudStatus == 'accept') {
                order.status = 'PROCESS'
            }
        } else if (transactionStatus == 'settlement') {
            order.status = 'PROCESS'
        } else if (transactionStatus == 'cancel' ||
            transactionStatus == 'deny' ||
            transactionStatus == 'expire') {
            order.status = 'CANCEL';

            //Revert Product Quantity
            const updateProductPromises = [];
            const orderProducts = await order_products.findAll({ where: { order_id: order.id }, include: "product" });
            orderProducts.forEach((orderProduct) => {
                updateProductPromises.push(
                    products.update({
                        qty: orderProduct.product.qty + orderProduct.qty
                    }, {
                        where: {
                            id: orderProduct.product.id
                        }
                    })
                )
            });
            await Promise.all(updateProductPromises);
        } else if (transactionStatus == 'pending') {
            order.status = 'PENDING'
        }
        await order.save();
        return { status: true, error: {}, data: order }

    } catch (err) {
        return { error: { message: err.message }, status: false, data: {} }
    }
}

module.exports = {
    receiveMidtransNotification
}